<?php
include_once ('vendor/autoload.php');
$student=new \App\student\student();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


</head>

<body>
<div class="container-fluid">

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Home</a></li>
                
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="view/student/studentlogin.php"><span class="glyphicon glyphicon-log-in"></span>  Student login</a></li>
              <li><a href="view/admin/login.php"><span class="glyphicon glyphicon-log-in"></span>  Admin login</a></li>
            </ul>
        </div>
    </nav>

    <div class="row">

        <div class="col-lg-6 col-sm-6 col-lg-offset-3 col-sm-offset-3">
            <h1>Submit student information</h1>

            <?php
            if(isset($_POST['submit'])){
                $name=$_POST['name'];
                $age=$_POST['age'];
                $email=$_POST['email'];
                $gender=$_POST['optradio'];
                $address=$_POST['address'];
                $dob=$_POST['year']."-".$_POST['month']."-".$_POST['day'];
                $password=$_POST['password'];
                $student->setName($name);
                $student->setAge($age);
                $student->setEmail($email);
                $student->setGender($gender);
                $student->setAddress($address);
                $student->setDob($dob);
                $student->setPassword($password);

                if($student->insertSrudent()==true){
                    echo "
                <div class='alert alert-success'>Data inserted !</div>";
                }else{
                    echo "<div class='alert alert-danger'>Invalid information or email may already exist !</div>";
                }
            }

            ?>
            <form action="" method="post">
                <div class="form-group">
                    <label for="name">Student name</label>
                    <input type="text" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="age">Age</label>
                    <input type="text" name="age" class="form-control">
                </div>
                
                
            <div class="form-group">
                <label for="email" >Enter your email:</label>
                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
            </div>

                
                
            <div class="form-group">
               <table>
                   <tr>
                       <td><label for="optradio">Select your gender : </label></td>
                       <td> <label class="radio-inline">
                    <input type="radio" name="optradio" value="male"> Male
                </label>
                  </td>
                  <td>
                <label class="radio-inline">
                     <input type="radio" name="optradio" value="female">Female
                </label> 
                  </td>
                   </tr>
               </table>
            </div>
               
               <div class="form-group">
                <label for="address" >Enter your Address:</label>
                <input type="text" class="form-control address" id="address" placeholder="Enter address" name="address">
            </div>

                
                
            <div class="form-group">
            <label for="" >Choose your Date of birth:</label>
               <table>
                   <tr>
                       <td><select class="form-control" id="date" name="day">
                    <option >Day</option>
                    <?php
                        for($i=1;$i<32;$i++){
                            echo "<option value='$i'>$i</option>";
                        }
                    ?>
                </select>
                  </td>
                      
                  <td>
                <select class="form-control" id="month" name="month">
                    <option >Month</option>
                    <?php
                    for($i=1;$i<=12;$i++){
                        echo "<option value='$i'>$i</option>";
                    }
                    ?>
                </select>
                  </td>
                  
                  <td>
                <select class="form-control" id="year" name="year">
                    <option >Year</option>";
                    <?php
                        for($i=1950;$i<=2017;$i++){
                            echo "<option value=$i>$i</option>";
                        }
                    ?>
                </select>
                      
                  </td>
                   </tr>
               </table>
                
            </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control">
                </div>

                <div class="form-group pull-right">
                    <button type="submit" class="btn btn-success" name="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>