<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/19/2017
 * Time: 1:30 PM
 */
namespace App\Session;
class Session
{

    public static function SessionInit(){
        if(version_compare(phpversion(),'5.4.0','<')){
            if(session_id==''){
                session_start();
            }
        }else
        {
            if(session_status()==PHP_SESSION_NONE)
            {
                session_start();
            }
        }

    }

    public static function set($key,$val){
            $_SESSION[$key]=$val;
    }

    public static function get($key){
        if(isset($_SESSION[$key])){
            return $_SESSION[$key];
        }else{
            return false;
        }
    }

    public static function Destroy(){
        session_destroy();
        session_unset();
        header('location:login.php');
    }

    public static function checksession(){
        if(self::get('login')==false){
            self::Destroy();
            header('Location:login.php');
        }
    }

    public static function StudentCheckSession(){
        if(self::get('login')==false){
            self::Destroy();
            header('Location:studentlogin.php');
        }
    }

    public static function checkSessionOn(){
        if(self::get('login')==true){
            header('location:addcourse.php');
        }
    }


}