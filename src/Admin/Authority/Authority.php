<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/18/2017
 * Time: 10:58 PM
 */

namespace  App\Admin\Authority;


use App\Session\Session;

class Authority
{

    private $email;
    private $password;



    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function AuthorityLogin(){

        if($this->email!='' && $this->password!='') {
            $sql = "select * from admin where email=:email and password=:password";
            $stmt = \App\Admin\DB\DB::myQuery($sql);
            $stmt->bindValue(':email', $this->email);
            $stmt->bindValue(':password', $this->password);

            $stmt->execute();
            $result=$stmt->fetch(\PDO::FETCH_OBJ);


            if ($stmt->rowCount()==1) {
                Session::SessionInit();
                Session::set('login',true);
                Session::set('email',$result->email);
                header('location:addcourse.php');
                return $result;
            } else {
                return false;
            }

        }

        else{
            return false;
        }

    }
    public function authorityLogout(){
        if(isset($_GET['action']) && ($_GET['action'])=='logout'){
            Session::Destroy();
            header('location:login.php');
        }
    }



}