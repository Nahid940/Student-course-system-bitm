<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/18/2017
 * Time: 10:09 PM
 */
namespace App\Admin\DB;
class DB
{
    private static $pdo;
    public static function myDb(){
        try{
            self::$pdo=new \PDO('mysql:host=localhost;dbname=studentcoursebitm',"root","");
        }catch (\PDOException $exp){
            return $exp->getMessage();
        }
        return self::$pdo;
    }

    public static function myQuery($query){
        return self::myDb()->prepare($query);
    }
}