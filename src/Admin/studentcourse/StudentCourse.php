<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/19/2017
 * Time: 12:36 AM
 */
namespace  App\Admin\studentcourse;
use App\Admin\DB\DB;
use App\Session\Session;

class StudentCourse
{
    private $sid;
    private $cid;
    private $assign='yes';

    /**
     * @param mixed $sid
     */
    public function setSid($sid)
    {
        $this->sid = $sid;
    }

    /**
     * @param mixed $cid
     */
    public function setCid($cid)
    {
        $this->cid = $cid;
    }

    public function AssignCourse(){
        $c=false;
        $totalarrayLnght=count($this->cid);
        for($i=0;$i<$totalarrayLnght;$i++){
           $sql="insert into studentcourse(sid,cid) VALUES (:sid,:cid)";
           $stmt=\App\Admin\DB\DB::myQuery($sql);
            $stmt->bindValue(':sid',$this->sid);
            $stmt->bindValue(':cid',$this->cid[$i]);
            $stmt->execute();
           $c=true;
        }
        if($c=true){
            $sql="update student set assign=:assign where sid=:sid";
            $stmt=\App\Admin\DB\DB::myQuery($sql);
            $stmt->bindValue(':assign',$this->assign);
            $stmt->bindValue(':sid',$this->sid);
            $stmt->execute();
        }
        return true;
    }


    public function getstudentAndCourseInfo(){
        $sql="select s.name,s.sid,c.cname from student s, course c,studentcourse stc where s.sid=stc.sid and
      c.cid=stc.cid and stc.cid=:cid";

        $stmt=\App\Admin\DB\DB::myQuery($sql);
        $stmt->bindValue(':cid',$this->cid);
        $stmt->execute();
        return $stmt->fetchAll();
    }


    public function GetAssignedCourseList(){
        $sql="select cname,ccode,duration from student s, course c,studentcourse stc where s.sid=stc.sid and
      c.cid=stc.cid and stc.sid=:sid";

        $stmt=\App\Admin\DB\DB::myQuery($sql);
        $stmt->bindValue(':sid',Session::get('sid'));
        $stmt->execute();
        return $stmt->fetchAll();
    }
}