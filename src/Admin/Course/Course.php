<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/18/2017
 * Time:
 * 11:03 PM
 */
namespace  App\Admin\Course;
class Course
{
    private $cname;
    private $ccode;
    private $duration;

    /**
     * @return mixed
     */
    public function getCname()
    {
        return $this->cname;
    }

    /**
     * @param mixed $cname
     */
    public function setCname($cname)
    {
        $this->cname = $cname;
    }

    /**
     * @return mixed
     */
    public function getCcode()
    {
        return $this->ccode;
    }

    /**
     * @param mixed $ccode
     */
    public function setCcode($ccode)
    {
        $this->ccode = $ccode;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }


    public function AddCourse(){
        if($this->cname !='' && $this->ccode!='' && $this->duration!='') {

            $sql = "insert into course (cname,ccode,duration) values(:cname,:ccode,:duration)";
            $stmt = \App\Admin\DB\DB::myQuery($sql);
            $stmt->bindValue(':cname', $this->cname);
            $stmt->bindValue(':ccode', $this->ccode);
            $stmt->bindValue(':duration', $this->duration);
            $stmt->execute();
            return true;
        }else{
            return false;
        }

    }

    public function GetAllCourseInformation(){
        $sql="select * from course";
        $stmt=\App\Admin\DB\DB::myQuery($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }




}