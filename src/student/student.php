<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/18/2017
 * Time: 9:44 PM
 */
namespace App\student;

use App\Admin\DB\DB;
use App\Session\Session;

class student
{

    private $name;
    private $age;
    private $email;
    private $gender;
    private $address;
    private $dob;
    private $password;

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
    private $assign='no';

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @param mixed $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }


    public function insertSrudent(){

        if($this->name!='' && $this->age != '' && $this->email!='' && $this->address!='' && $this->gender!='' && $this->address!='' && $this->dob!='') {

            $sql="select email from student where email=:email";
            $stmt=DB::myQuery($sql);
            $stmt->bindValue('email',$this->email);
            $stmt->execute();
            if($stmt->rowCount()==0) {
                $sql = "insert into student (name,age,email,gender,address,dob,assign,password) values(:name,:age,:email,:gender,:address,:dob,:assign,:password)";
                $stmt = \App\Admin\DB\DB::myQuery($sql);
                $stmt->bindValue(':name', $this->name);
                $stmt->bindValue(':age', $this->age);
                $stmt->bindValue(':email', $this->email);
                $stmt->bindValue(':gender', $this->gender);
                $stmt->bindValue(':address', $this->address);
                $stmt->bindValue(':dob', $this->dob);
                $stmt->bindValue(':assign', $this->assign);
                $stmt->bindValue(':password', $this->password);
                $stmt->execute();
                return true;
            }else
                return false;

        }
    }


    public function GetAllStudentInformation(){
        $sql="select * from student where assign='no'";
        $stmt=\App\Admin\DB\DB::myQuery($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }


    public function studentlogin(){
        $sql="select sid,name,email,password from student where email=:email and password=:password";
        $stmt=\App\Admin\DB\DB::myQuery($sql);
        $stmt->bindValue(':email',$this->email);
        $stmt->bindValue(':password',$this->password);
        $stmt->execute();
        $result=$stmt->fetch(\PDO::FETCH_OBJ);
        if($stmt->rowCount()==1){
            Session::SessionInit();
            Session::set('login',true);
            Session::set('sid',$result->sid);
            Session::set('email',$result->email);
            Session::set('name',$result->name);

            header('location:studentPortal.php');
            return $result;
        }else{
            return false;
        }
    }

    public function setLogout(){
        if(isset($_GET['action']) && ($_GET['action'])=='logout'){
            Session::Destroy();
            header('location:studentlogin.php');
        }
    }







}