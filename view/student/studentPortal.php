<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/19/2017
 * Time: 6:51 PM
 *
 */
include_once '../../vendor/autoload.php';
$student=new \App\student\student();
$studentcourse=new \App\Admin\studentcourse\StudentCourse();
\App\Session\Session::SessionInit();

$student->setLogout();
$name=\App\Session\Session::get('name');


\App\Session\Session::checksession();
\App\Session\Session::StudentCheckSession();


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


</head>

<body>
<div class="container-fluid">

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Home</a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="?action=logout"> logout</a></li>
            </ul>
        </div>
    </nav>


    <div class="row">
        <div class="col-md-12">
            <h2> Welcome <?php echo $name?></h2>
            <h3>Courses that you are assigned:</h3>

            <table class="table">
                <tr>
                    <th>Course Name</th>
                    <th>Course ID</th>
                    <th>Course duration</th>
                </tr>

                <?php
                    foreach ($studentcourse->GetAssignedCourseList() as $studentCourseInfo){


                ?>
                <tr>
                    <td><?php echo $studentCourseInfo['cname']?></td>
                    <td><?php echo $studentCourseInfo['ccode']?></td>
                    <td><?php echo $studentCourseInfo['duration']?></td>
                </tr>
                <?php }?>
            </table>
        </div>
    </div>


</div>
</body>
</html>
