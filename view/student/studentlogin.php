<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/19/2017
 * Time: 6:38 PM
 */
?>

<?php
include_once('../../vendor/autoload.php');
$student=new \App\student\student();




?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


</head>

<body>
<div class="container-fluid">

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <ul class="nav navbar-nav">
                <li class="active"><a href="../../index.php">Home</a></li>

            </ul>
        </div>
    </nav>

    <div class="row">
        <div class="col-lg-6 col-sm-6 col-lg-offset-3 col-sm-offset-3">
            <h2>Student login</h2>
            <?php
            if(isset($_POST['login'])){
                $email=$_POST['email'];
                $password=$_POST['password'];
                $student->setEmail($email);
                $student->setPassword($password);

                if(!$student->studentlogin()){
                    echo "<div class='alert alert-danger'>Invalid login</div>";
                }
            }
            ?>

            <form action="" method="post">


                <div class="form-group">
                    <label for="email" >Enter email:</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                </div>


                <div class="form-group">
                    <label for="password" >Enter password:</label>
                    <input type="password" class="form-control" id="email" placeholder="Enter password" name="password">
                </div>


                <div class="form-group pull-right">
                    <button type="submit" class="btn btn-success" name="login">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
