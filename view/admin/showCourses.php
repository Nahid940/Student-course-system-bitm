<?php
include_once('../../vendor/autoload.php');
$course=new \App\Admin\Course\Course();
$authority=new \App\Admin\Authority\Authority();
\App\Session\Session::SessionInit();
\App\Session\Session::checksession();
$authority->authorityLogout();
include_once  'include/header.php';

?>



<body>
<div class="container-fluid">

      <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <ul class="nav navbar-nav">
                <li class=""><a href="view/admin/addcourse.php">Add Course</a></li>
                <li><a href="view/admin/showCourses.php">View course</a></li>
                <li><a href="view/admin/assigncourse.php">Assign course</a></li>
            </ul>
            
            <ul class="nav navbar-nav navbar-right">
                <li><a href="?action=logout"><span class="glyphicon glyphicon-log-in"></span>  Logout</a></li>
            </ul>
        </div>
    </nav>

    <div class="row">

        <div class="col-lg-6 col-sm-6 col-lg-offset-3 col-sm-offset-3">
            <h1>Assign course</h1>
            <table class="table">
                <tr>
                    <th>Sl.</th>
                    <th>Course title</th>
                    <th>Action</th>
                </tr>
                <?php
                $i=0;

                    foreach($course->GetAllCourseInformation() as $course)
                    {
                    $i++;
                ?>
                <tr>
                    <td><?php echo $i?></td>
                    <td><?php echo $course['cname']?></td>
                    <td><a href="view/admin/viewinfo.php?id=<?php echo $course['cid']?>&name=<?php echo $course['cname']?>" class="btn btn-info">View info</a></td>
                </tr>
                <?php
                    }
                ?>

            </table>
        </div>
    </div>
    <?php include_once 'include/footer.php'?>
</div>
</body>
</html>