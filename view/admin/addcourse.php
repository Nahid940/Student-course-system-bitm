<?php
include_once('../../vendor/autoload.php');

$courseObj=new \App\Admin\Course\Course();


$authority=new \App\Admin\Authority\Authority();

\App\Session\Session::SessionInit();

\App\Session\Session::checksession();
//\App\Session\Session::checkSessionOn();
$authority->authorityLogout();

//$email=\App\Session\Session::get('email');
include_once  'include/header.php';
?>



<body>
<div class="container-fluid">

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <ul class="nav navbar-nav">
                <li class=""><a href="view/admin/addcourse.php">Add Course</a></li>
                <li><a href="view/admin/showCourses.php">View course</a></li>
                <li><a href="view/admin/assigncourse.php">Assign course</a></li>
            </ul>
            
            <ul class="nav navbar-nav navbar-right">
                <li><a href="?action=logout"><span class="glyphicon glyphicon-log-in"></span>  Logout</a></li>
            </ul>
        </div>
    </nav>

    <div class="row">

        <div class="col-lg-6 col-sm-6 col-lg-offset-3 col-sm-offset-3">
            <h1>Add new course</h1>
            <?php

            if(isset($_POST['submit'])) {
                $cname = $_POST['cname'];
                $ccode = ($_POST['ccode']);
                $duration = ($_POST['duration']);

                $courseObj->setCname($cname);
                $courseObj->setCcode($ccode);
                $courseObj->setDuration($duration);
                if($courseObj->AddCourse()){
                    echo "<div class='alert alert-success'>Data inserted</div>";
                }else{
                    echo "<div class='alert alert-danger'>Data not inserted</div>";
                }

            }

            ?>
            <form action="" method="post">
                <div class="form-group">
                    <label for="cname">Course name</label>
                    <input type="text" name="cname" class="form-control">
                </div>
                <div class="form-group">
                    <label for="ccode">Course code</label>
                    <input type="text" name="ccode" class="form-control">
                </div>
                <div class="form-group">
                    <label for="duration">Course duration</label>
                    <input type="text" name="duration" class="form-control">
                </div>
                <div class="form-group pull-right">
                    <button type="submit" class="btn btn-success" name="submit">Add course</button>
                </div>
            </form>
        </div>
    </div>

    <?php include_once 'include/footer.php'?>
</div>
</body>
</html>