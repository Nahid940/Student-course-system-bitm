<?php
include_once('../../vendor/autoload.php');
 $student=new \App\student\student();
$course=new \App\Admin\Course\Course();

$authority=new \App\Admin\Authority\Authority();
\App\Session\Session::SessionInit();
\App\Session\Session::checksession();
$authority->authorityLogout();

include_once  'include/header.php';

?>



<body>
<div class="container-fluid">

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <ul class="nav navbar-nav">
                <li class=""><a href="view/admin/addcourse.php">Add Course</a></li>
                <li><a href="view/admin/showCourses.php">View course</a></li>
                <li><a href="view/admin/assigncourse.php">Assign course</a></li>
            </ul>
            
            <ul class="nav navbar-nav navbar-right">
                <li><a href="?action=logout"><span class="glyphicon glyphicon-log-in"></span>  Logout</a></li>
            </ul>
        </div>
    </nav>

    <div class="row">

    <div class="col-lg-6 col-sm-6 col-lg-offset-3 col-sm-offset-3">
        <h1>Assign course</h1>
        <?php
            if(isset($_POST['submit'])) {
            $stc=new \App\Admin\studentcourse\StudentCourse();
            $sid=$_POST['student'];
            $AssignedCourse=$_POST['course'];

            $stc->setSid($sid);
            $stc->setCid($AssignedCourse);

            if($stc->AssignCourse()){
                echo "<div class='alert alert-success'>Course assigned</div>";
            }else{
                    echo "<div class='alert alert-danger'>Course not assigned !</div>";
                }

            }
        ?>
        <form action="" method="post">
            <div class="form-group">
                <label for="sel1">Select student :</label>
                <select class="form-control" id="student" name="student">
                    <option value="">Select student</option>
                    <?php

                    foreach($student->GetAllStudentInformation() as $student){
                    ?>
                    <option value="<?php echo $student['sid']?>" name="student"><?php echo $student['name']?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="course">Select course : </label>
                <?php

                foreach($course->GetAllCourseInformation() as $course){
                    ?>
                    <label class="checkbox-inline"><input type="checkbox" value="<?php echo $course['cid']?>" name="course[]"><?php echo $course['cname']?></label>
                    <?php
                }
                ?>
            </div>
            <div class="form-group pull-right">
                <button type="submit" name="submit" class="btn btn-success">Assign course</button>
            </div>
        </form>
    </div>
    </div>
    <?php include_once 'include/footer.php'?>
</div>
</body>
</html>