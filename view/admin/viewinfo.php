<?php
include_once('../../vendor/autoload.php');
$studentcourse=new \App\Admin\studentcourse\StudentCourse();

    if(isset($_GET['id']) && isset($_GET['name'])){
        $cid=$_GET['id'];
        $cname=$_GET['name'];

        $studentcourse->setCid($cid);


    }

$authority=new \App\Admin\Authority\Authority();
\App\Session\Session::SessionInit();
\App\Session\Session::checksession();
$authority->authorityLogout();

include_once  'include/header.php';
?>


<div class="container-fluid">

      <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <ul class="nav navbar-nav">
                <li class=""><a href="view/admin/addcourse.php">Add Course</a></li>
                <li><a href="view/admin/showCourses.php">View course</a></li>
                <li><a href="view/admin/assigncourse.php">Assign course</a></li>
            </ul>
            
            <ul class="nav navbar-nav navbar-right">
                <li><a href="?action=logout"><span class="glyphicon glyphicon-log-in"></span>  Logout</a></li>
            </ul>
        </div>
    </nav>

    <div class="row">

        <div class="col-lg-6 col-sm-6 col-lg-offset-3 col-sm-offset-3">
            <h1>Assigned students on <?php echo $cname?></h1>
            <table class="table">
                <tr>
                    <th>Sl.</th>
                    <th>Student name</th>
                    <th>Student ID</th>
                    <th>Course name</th>
                </tr>
                <?php
                $i=0;
                foreach($studentcourse->getstudentAndCourseInfo() as $courseInfo)
                {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i?></td>
                        <td><?php echo $courseInfo['name']?></td>
                        <td><?php echo $courseInfo['sid']?></td>
                        <td><?php echo $courseInfo['cname']?></td>
                    </tr>
                    <?php
                }
                ?>

            </table>
        </div>
    </div>
    <?php include_once 'include/footer.php'?>
</div>
</body>
</html>
