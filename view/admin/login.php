<?php
include_once('../../vendor/autoload.php');
$authority=new \App\Admin\Authority\Authority();


include_once  'include/header.php';

?>


<body>
<div class="container-fluid">

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <ul class="nav navbar-nav">
                <li class="active"><a href="../../index.php">Home</a></li>
               
            </ul>
            

        </div>
    </nav>

    <div class="row">
        <div class="col-lg-6 col-sm-6 col-lg-offset-3 col-sm-offset-3">
            <h1>Admin login</h1>
            <?php
            if(isset($_POST['login'])){
                $email=$_POST['email'];
                $password=$_POST['password'];
                $authority->setEmail($email);
                $authority->setPassword($password);
                if(!$authority->AuthorityLogin()){
                    echo "<div class='alert alert-danger'>Invalid login</div>";
                }else{
                    \App\Session\Session::SessionInit();
                }
            }
            ?>

            <form action="" method="post">
            
            <div class="form-group">
                <label for="email" >Enter email:</label>
                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
            </div>

                
                
            <div class="form-group">
                <label for="password" >Enter password:</label>
                <input type="password" class="form-control" id="email" placeholder="Enter password" name="password">
            </div>
               
              
                <div class="form-group pull-right">
                    <button type="submit" class="btn btn-success" name="login">Login</button>
                </div>
            </form>
        </div>
    </div>
    <?php include_once 'include/footer.php'?>
</div>
</body>
</html>